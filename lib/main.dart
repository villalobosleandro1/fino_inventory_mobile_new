
import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/containers/listGoods.dart';
import 'package:fino_inventory_mobile/src/containers/menu.dart';
import 'package:fino_inventory_mobile/src/containers/movementDetail.dart';
import 'package:fino_inventory_mobile/src/containers/movementsHistory.dart';
import 'package:fino_inventory_mobile/src/containers/qrInformation.dart';
import 'package:fino_inventory_mobile/src/containers/registerMovements.dart';
import 'package:flutter/material.dart';
import 'package:fino_inventory_mobile/src/containers/login.dart';
import 'package:fino_inventory_mobile/src/containers/dashboard.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  final routes = <String, WidgetBuilder> {
    'Login': (BuildContext context) => Login(),
    'Dashboard': (BuildContext context) => Dashboard(),
    'MovementDetail': (BuildContext context) => MovementDetail(),
    'RegisterMovement': (BuildContext context) => RegisterMovement(),
    'MovementsHistory': (BuildContext context) => MovementsHistory(),
    'Menu': (BuildContext context) => Menu(),
    'ListGoods': (BuildContext context) => ListGoods(),
    'QrInformation': (BuildContext context) => QrInformation()
  };

  bool isLoged = false;
  final storage = new FlutterSecureStorage();
  Map<String, String> allValues;

  initState() {
    super.initState();
    _read();
  }

  _read() async {
    
        allValues = await storage.readAll();
        if(allValues['token'] != null) {
          setState(() {
            isLoged = true;
          });
        }
      
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CodeCraft',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: API.colors['amarillo']
      ),
      home: isLoged == true ? Menu() : Login(),
      routes: routes,
    );
  }
}

