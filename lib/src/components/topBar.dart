import 'package:fino_inventory_mobile/src/containers/dashboard.dart';
import 'package:fino_inventory_mobile/src/containers/menu.dart';
import 'package:fino_inventory_mobile/src/containers/movementsHistory.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fino_inventory_mobile/src/components/api.dart';

class TopBar extends StatefulWidget {
  bool hideBackButton;


  @override
  _TopBarState createState() => _TopBarState();
  TopBar({Key key, this.hideBackButton}) : super(key: key);
}

class _TopBarState extends State<TopBar> with SingleTickerProviderStateMixin {
  // MenuOption _itemSelected = items[0]; 
  
  
  // void _select(MenuOption item) async {
  //   if(item.title == 'Logout'){
  //     final storage = new FlutterSecureStorage();
  //     await storage.deleteAll();
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
  //   }
  // }

  _logOut() {
    // Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
    Navigator.of(context).pushNamedAndRemoveUntil('Login', (Route<dynamic> route) => false);
  }

  _exitApp() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you really want to exit the app?'),
        actions: <Widget>[
          FlatButton(
            child: Text('No'),
            onPressed: () =>Navigator.pop(context, false),
          ),
          FlatButton(
            child: Text('Yes'),
            onPressed: () async {
              final storage = new FlutterSecureStorage();
              await storage.deleteAll();
              Navigator.pop(context, true);
              _logOut();
            },
          ),
        ],
      )
    );
  }

  Widget backButton() {
    if(widget.hideBackButton == false) {
      return IconButton(icon:Icon(Icons.arrow_back),
              onPressed:() => Navigator.push(context, MaterialPageRoute(builder: (context) => Menu())),
            );  
    }else {
      return null;
    }
  }
  
  @override
  Widget build(BuildContext context) {
     
    return AppBar(
          backgroundColor: API.colors['amarillo'],
          title: const Text('FINO INVENTORY', style: TextStyle(fontSize: 14),), 
           
          leading:  this.backButton(),
          actions: <Widget>[
            IconButton( 
              icon: Icon(items[0].icon),
              onPressed: () { 
                Navigator.of(context).pushNamedAndRemoveUntil('Menu', (Route<dynamic> route) => false);
                // _select(items[1]); 
              },
            ),
            // IconButton( 
            //   icon: Icon(items[1].icon),
            //   onPressed: () { 
            //     Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
            //     // Navigator.of(context).pushNamedAndRemoveUntil('MovementsHistory', (Route<dynamic> route) => false);
            //     // _select(items[1]); 
            //   },
            // ),
            IconButton( 
              icon: Icon(items[2].icon),
              onPressed: () { 
                _exitApp();
                // Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
                // _select(items[3]); 
              },
            ),
            // PopupMenuButton<MenuOption>( 
            //   onSelected: _select,
            //   itemBuilder: (BuildContext context) {
            //     return items.skip(2).map<PopupMenuItem<MenuOption>>((MenuOption choice) {
            //       return PopupMenuItem<MenuOption>(
            //         value: choice,
            //         child: ListTile(
            //           title: Text(choice.title),
            //           leading: IconButton(
            //             icon: Icon(Icons.exit_to_app,
            //             color: API.colors['amarillo']),
            //             onPressed: () {},
            //           ),
            //         ),
            //       );
            //     }).toList();
            //   },
            // ),
          ],
        ); 
  }

}

class MenuOption {
  const MenuOption({ this.title, this.icon });
  final String title;
  final IconData icon;
}

const List<MenuOption> items = <MenuOption>[
  
  MenuOption(title: 'Home', icon: Icons.home),
  MenuOption(title: 'History', icon: Icons.history),
  MenuOption(title: 'Logout', icon: Icons.exit_to_app),
];
class menuOptionsCard extends StatelessWidget {
  const menuOptionsCard({ Key key, this.choice }) : super(key: key);

  final MenuOption choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 128.0, color: textStyle.color),
            Text(choice.title, style: textStyle),
          ],
        ),
      ),
    );
  }
}
