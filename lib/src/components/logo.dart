import 'package:flutter/material.dart';

class Logo extends StatelessWidget {

  Logo({@required this.name});
  final name;

  @override
  Widget build(BuildContext context) {
    
    return Container(
              height: 250,
              alignment: Alignment.bottomCenter,
              child: Image(
                alignment: Alignment.bottomCenter,
                image: AssetImage(name),
                height: 250,
                width: 200,
              ),
            );
  }
}