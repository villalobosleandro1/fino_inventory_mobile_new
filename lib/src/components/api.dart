import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class API {
  // https://api.inventory.futurapps.com/
  static const titleApp = 'FINO INVENTORY';
  static const url = 'api.inventory.futurapps.com';
  static var colors = {
    'amarillo': Color.fromRGBO(252, 199, 19, 1.0),
    'rosado': Color.fromRGBO(229, 162, 164, 1.0),
    'verde': Color.fromRGBO(79, 169, 83, 1.0),
    'celeste': Color.fromRGBO(14, 181, 202, 1.0),
    'rojo': Color.fromRGBO(233, 67, 63, 1.0),
    'white': Color.fromRGBO(255, 255, 255, 1.0),
    'redDanger': Color.fromRGBO(220, 53, 69, 1.0),
    'gray': Color.fromRGBO(128, 130, 133, 1.0),
    'greenSuccess': Color.fromRGBO(40, 167, 69, 1.0)
  };

  static var image = {
    'logo': 'lib/src/assets/images/logo.png',
    'noResults': 'lib/src/assets/images/no-results.png',
  };

  static var apiRoutes = {
    'GOODS': '/goods/',
    'userData': '/user-data/',
    'movements': '/movements/',
    'movementsDestination': '/movements?destination=',
    'login': '/login/',
    'agencies': '/agencies/?area=true',
    'getUserByAgency' : '/agency-users/?agencyId=',
    'getQrForAgency': '/goodsqr-list/?agency=',
    'createQr': '/goodsqr/',
    'goodsList': '/goods-list/',
    'goodsById' : '/goodsqr-list/?goodId='
  };

  static login(email, pass) async {
    //   final aux = Uri.http(url, apiRoutes['login']);
    //   print('\x1B[35m' + '=====================================' + '\x1B[0m');
    //   print(aux);
    //   print('\x1B[35m' + '=====================================' + '\x1B[0m');
      
    // final response = await http.post(aux, body: {"usernameOrEmail": email, "password": pass}, headers: {"Accept":"application/json"});
    //   print('\x1B[35m' + '=====================================' + '\x1B[0m');
    //   print(json.decode(response.body));
    //   print('\x1B[35m' + '=====================================' + '\x1B[0m');
    // return response;

    var url = "https://api.inventory.futurapps.com/login/";

    final response = await http.post(url, body: {"usernameOrEmail": email, "password": pass}, headers: {"Accept":"application/json"});
    // print('\x1B[35m' + '=====================================' + '\x1B[0m');
    //   print(json.decode(response.body));
    //   print('\x1B[35m' + '=====================================' + '\x1B[0m');
    // final responseJson = json.decode(response.body);

    return response;
  }

  static Future getGoods() async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    final urlDashboard = Uri.http(url, apiRoutes['GOODS']);
    // print(urlDashboard);
    
    return http.get(urlDashboard, headers: {"Authorization": token});
  }

  static getUserInfo() async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    final aux = Uri.http(url, apiRoutes['userData']);
    return http.get(aux, headers: {"Authorization": token});
  }

  static createMovements(data) async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    var body = json.encode(data['data']);

    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
      "Authorization": token
    };

    var url = "https://api.inventory.futurapps.com/movements/";

    final response = await http.post(url, body: body, headers: headers);
    final responseJson = json.decode(response.body);

    return responseJson;
  }

  static getMovements(data) async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    var id = data['id'];
    var urlMovements = Uri.http(url, apiRoutes['movements'] + id);


    if(data['type'] == 'list') {
      var id = data['id'];
      // var page = data['page'];
      urlMovements = Uri.parse("https://api.inventory.futurapps.com/movements?agency_id=$id&status=S");
    }

    return http.get(urlMovements, headers: {"Authorization": token});
    
  }

  static checkQr(data) async {
    var qr = data['qr'];
    bool custom = data['custom'];
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];

    var urlMovements = Uri.parse("https://api.inventory.futurapps.com/read-qr/?qr=$qr&custom=$custom");
    print('*******************************');
    print(urlMovements);
    
    return http.get(urlMovements, headers: {"Authorization": token});
  }

  static checkMovement(data, id) async {


    final storage = FlutterSecureStorage();
    Map<String, String> allValues;
    allValues = await storage.readAll();
    var token = allValues['token'];

    // example 1
    var aux = Uri.http(url, apiRoutes['movements'] + id +'/'); 
    Map<String, String> headers = {"Content-type": "application/json", "Authorization": token};
    Response response = await put(aux, headers: headers, body: json.encode(data));

    return response;

  }

  static getAgencies() async {

    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    final urlDashboard = Uri.parse("https://api.inventory.futurapps.com/agencies/?area=true");
    return http.get(urlDashboard, headers: {"Authorization": token});
  }

  static getInfoAgency(id) async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    final urlDashboard = Uri.parse("https://api.inventory.futurapps.com/agencies/$id/?area=true");
    return http.get(urlDashboard, headers: {"Authorization": token});
  }

  static getQrForAgencyId(agencyId) async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    final urlDashboard = Uri.parse("https://api.inventory.futurapps.com/goodsqr-list/?agency=$agencyId");
    return http.get(urlDashboard, headers: {"Authorization": token});
  }

  static getUserByAgency(agencyId) async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    final urlDashboard = Uri.parse("https://api.inventory.futurapps.com/agency-users/?agencyId=$agencyId");
    return http.get(urlDashboard, headers: {"Authorization": token});
  }

  static getMovementsHistory(data) async {
    
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    var id = data['id'];
    var urlMovements = Uri.parse("https://api.inventory.futurapps.com/movements/?agency_id=$id");
    

    if((data['filter'] != null) && (data['filter'] != 'All')) {
      var filter;

      switch (data['filter']) {
        case 'Sent':
          filter = 'S';
          break;
        case 'Received': 
          filter = 'R';
          break;

        case 'Delivered':
          filter = 'D';
          break;
      }
      urlMovements = Uri.parse("https://api.inventory.futurapps.com/movements/?agency_id=$id&status=$filter");
    }

    return http.get(urlMovements, headers: {"Authorization": token});
    
  }

  static createQr(data) async {
    
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var token = allValues['token'];
    var body = json.encode(data);

    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
      "Authorization": token
    };

    // print('body $body');
    

    var url = "https://api.inventory.futurapps.com/goodsqr/";

    final response = await http.post(url, body: body, headers: headers);
    final responseJson = json.decode(response.body);

    return responseJson;
  }

  static getGoodsList() async {
      final storage = FlutterSecureStorage();
      Map<String, String> allValues;

      allValues = await storage.readAll();
      var token = allValues['token'];
      final urlDashboard = Uri.parse("https://api.inventory.futurapps.com/goods-list/");
      return http.get(urlDashboard, headers: {"Authorization": token});
  }

  static goodsById(goodId) async {
      final storage = FlutterSecureStorage();
      Map<String, String> allValues;

      allValues = await storage.readAll();
      var token = allValues['token'];
      final urlDashboard = Uri.parse("https://api.inventory.futurapps.com/goodsqr-list/?goodId=$goodId");
      return http.get(urlDashboard, headers: {"Authorization": token});
  }

   static detailGood(goodId) async {
      final storage = FlutterSecureStorage();
      Map<String, String> allValues;

      allValues = await storage.readAll();
      var token = allValues['token'];
      final urlDashboard = Uri.parse("https://api.inventory.futurapps.com/goods/$goodId");
      return http.get(urlDashboard, headers: {"Authorization": token});
  }








}
