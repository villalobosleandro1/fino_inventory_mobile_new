import 'dart:convert';

import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'package:fino_inventory_mobile/src/containers/goodDetail.dart';
import 'package:flutter/material.dart';


void main() => runApp(ListGoods());

class ListGoods extends StatefulWidget {
  @override
  _ListGoodsState createState() => _ListGoodsState();
}

class _ListGoodsState extends State<ListGoods> {
  bool loading = true;
  var listGoods = [];
 
  @override
  void initState() {
    super.initState();
    _getGoods();
  }

  _getGoods() {
    API.getGoods().then((goods) async {
      var aux =  json.decode(goods.body);
      // print('*****************************');
      // print(aux);
      
      setState(() {
       listGoods = aux['results'];
       loading = false;
      });
    });
  }

  // _getInfoWithGoodId() {

  // }

  Widget _buildBody() {
    
    if(loading == true) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }else {
      if(listGoods.length == 0) {
        return Center(
          child: Text('There are no goods'),
        );
      }else {
        return ListView.builder(
          itemCount: listGoods.length,
          itemBuilder: (context, index) {
            return
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: (
                InkWell(
                  onTap: (){
                    Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => GoodDetail(
                                goodId: listGoods[index]['id'],
                                nameGood: listGoods[index]['name'],
                                categoryGood: listGoods[index]['category']['name'],
                                subCategoryGood: listGoods[index]['subcategory']['name']
                              ),
                            ),
                          );
                  },
                  child: Container(
                    width: 380.0,
                    height: 130.0,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        // begin: Alignment.topCenter,
                        colors: [Colors.black87, API.colors['amarillo']],
                        // stops: [0.0, 0.5]
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 1.0,
                          color: Colors.black12,
                          spreadRadius: 2.0
                        )
                      ]
                    ),

                    child: Container(
                      padding: EdgeInsets.only(left: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Text(listGoods[index]['name'],  style: TextStyle(fontSize: 16.0, color: Colors.white)),
                          ),

                           Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: Container(
                              // color: Colors.pinkAccent,
                              // width: 70,
                              // height: 130,
                              alignment: Alignment.center,
                              child: Image.asset(
                                'lib/src/assets/images/box_close.png',
                                height: 70.0,
                                ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
                
              ),
            );
          }
        );
      }
      
    }
  }

  @override
  Widget build(BuildContext context) {
    
    
    return Scaffold(
        appBar: AppBar(
         flexibleSpace: TopBar(hideBackButton: false),
        ),
        body: 
        _buildBody(), 
      );
  }


}


