import 'dart:convert';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';

import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:animated_floatactionbuttons/animated_floatactionbuttons.dart';
import 'package:toast/toast.dart';


import 'dashboard.dart';

void main() {
  runApp(Center(child: MovementDetail()));
}

class MovementDetail extends StatefulWidget {
  String value;
  String delivered;
  var user;
  var icono;
  
  MovementDetail({Key key, this.value, this.user, this.icono, this.delivered}) : super(key: key);

  static get tag => null;
 
  _MovementDetailState createState() => _MovementDetailState();
}

class _MovementDetailState extends State<MovementDetail> with SingleTickerProviderStateMixin {
 
  bool loading;
  dynamic listDetail;
  int countProductChecked = 0;
  
  var arreglo = [];
  var goods = [];
  
  initState() {
    super.initState();
    loading = true;
    _getMovementDetail();
  }

  _getMovementDetail() {
    
      var aux = {
        'type': 'detail',
        'id': widget.value
      };

      
      API.getMovements(aux).then((movements) async {
        listDetail = json.decode(movements.body);
        if(listDetail['detail']['success'] == 'False') {
          Toast.show("Session expired", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
          final storage = new FlutterSecureStorage();
          await storage.deleteAll();
          // Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
          Navigator.of(context).pushNamedAndRemoveUntil('Login', (Route<dynamic> route) => false);
        }else {
          // print('listDetail');
          var limiteFor = listDetail['detail']['data']['goods_qrs'];
          // print(limiteFor.length);

        
          for(int x = 0; x < limiteFor.length; x++) {
            var lista = Map(); 
            lista['name'] = limiteFor[x]['good']['name'];
            lista['id'] = limiteFor[x]['id'];
            lista['serialQR'] = limiteFor[x]['serialQR'];
            lista['check'] = false;
            lista['base64'] = '';
            lista['serial'] = limiteFor[x]['serial'];
            arreglo.add(lista);
          }
          
          setState(() {
            loading = false;
          });
        }
      
      
      });
   

  }

  uploadGoods(goods) {
    // widget.value este es el id del movimiento    

    if(countProductChecked != arreglo.length) {
      return showDialog<void>(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Alert!'),
                    content: const Text('All products of the movement have not been scanned'),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Ok'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  );
                },
              );
    }else {
      setState(() {
       loading = true; 
      });
      
      dynamic respuestaMovement;
      
      var movementId = widget.value;

      var now = DateTime.now();
      var formatter = DateFormat('yyyy-MM-dd hh:mm');
      String formatted = formatter.format(now);
        

      var informacion = Map(); 
      informacion['goods_qr'] = [];
      informacion['reception_date'] = formatted;

      for(var i = 0; i < goods.length; i++) {
        if(goods[i]['check'] == true) {
          informacion['goods_qr'].add(goods[i]['base64']);
        }
      }


      API.checkMovement(informacion, movementId).then((responseMovement) {
        respuestaMovement = json.decode(responseMovement.body);
        // print('***************************');
        // print(respuestaMovement['detail']);
      
        if(respuestaMovement['detail']['success'] == true) {
          setState(() {
           loading = false; 
          });
          return showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Success'),
                content: const Text('The movement has been update correctly'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
                    },
                  ),
                ],
              );
            },
          );
        }else {
          return showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Error'),
                content: const Text('An error has occurred please try again'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MovementDetail(value: widget.value),
                          ),
                        );
                    },
                  ),
                ],
              );
            },
          );
        }
      
      });
    }
    
  }

  checkBarcode(barcode) {
    
    API.checkQr(barcode).then((response) {
          
      dynamic respuesta = json.decode(response.body);
    
      if(respuesta['detail']['data']['serialQR'] != null) {
        
        for(int i = 0; i < arreglo.length; i++) {

          if(arreglo[i]['serialQR'] == respuesta['detail']['data']['serialQR']) {

            if(arreglo[i]['check'] == true){
              
              return showDialog<void>(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Alert!'),
                    content: const Text('the qr code was already scanned'),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Ok'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  );
                },
              );
            }else{
              
              countProductChecked++;
              setState(() {
                arreglo[i]['check'] = !arreglo[i]['check']; 
                arreglo[i]['serialQR'] = listDetail['detail']['data']['goods_qrs'][i]['serialQR']; 
                arreglo[i]['base64'] = barcode;
              });
              Toast.show("Qr scanned successfully", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
              scan();
            }
          
          }
        
        }

      }else {
        Toast.show("The scanned product was not found in the database", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }

    });
  }
  
  Future scan() async {
    try{
      // aqui entra cuando si scanea algo
      String barcode = await BarcodeScanner.scan();
      // goods.add(barcode);
      // setState(() => this.barcode = barcode);
      checkBarcode(barcode);
    } on PlatformException catch(e) {
      // print('111111111111111111111');
      if(e.code == BarcodeScanner.CameraAccessDenied) {
        // print('1...............1');
        Toast.show("The user did not grant the camera permission", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

        // setState(() {
        //  this.barcode = 'The user did not grant the camera permission'; 
        // });
      }else {
        // print('1..................2');
        Toast.show("Unknow error: $e", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        // setState(() => this.barcode = 'Unknow error: $e');
      }
    } on FormatException {
      // aqui es cuando le doy al boton de atras sin scanear nada
      // print('2222222222222222222');
      // Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()),);
      // setState(() => this.barcode = 'User returned using the back button before scanning anythin');
    } catch(e) {
      // print('3333333333333333333333');
      Toast.show("Unknow error: $e", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      // setState(() => this.barcode = 'Unknow error $e');
    }
  }

  Widget takeQr() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: API.colors['amarillo'],
        onPressed: (){scan();},
        heroTag: null,
        tooltip: 'Take qr',
        child: Icon(Icons.center_focus_weak),
      ),
    );
}
  
  Widget upload() {
      return Container(
        child: FloatingActionButton(
          backgroundColor: API.colors['amarillo'],
          onPressed: (){uploadGoods(arreglo);},
          heroTag: null,
          tooltip: 'Upload',
          child: Icon(Icons.file_upload),
        ),
      );
  }

  Widget _buildBody() {
   
    if (loading) {
     return Container(
        color: Colors.white,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    } else {
      return Center (
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10, right: 10, left: 10),
                      child: Container(
                        width: 380,
                        height: 130.0,
                        
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 1.0,
                              color: Colors.black12,
                              spreadRadius: 2.0
                            )
                          ]
                        ),

                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('N. movement: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                                      Text(listDetail['detail']['data']['id_movement'], style: TextStyle(fontSize: 16.0))
                                    ],
                                  ),
                                  Icon(Icons.format_list_numbered_rtl, color: API.colors['amarillo']),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('Issuing user: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                                      Text(listDetail['detail']['data']['issuing_user']['first_name'], style: TextStyle(fontSize: 14.0))
                                    ],
                                  ),
                                  
                                  Icon(Icons.local_offer, color: API.colors['amarillo']),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('Receiving user: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                                      Text(listDetail['detail']['data']['receiving_user']['id'] == widget.user['id'] ? listDetail['detail']['data']['receiving_user']['first_name'] : widget.user['first_name'], style: TextStyle(fontSize: 14.0))
                                    ],
                                  ),
                                  
                                  Icon(Icons.supervised_user_circle, color: API.colors['amarillo']),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('Type movement: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                                  Icon(widget.icono, color: API.colors['amarillo']),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    //aqui puedo agregar la seccion del contador de porductos scaneados
                    
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, right: 12.0, top: 10.0),
                      
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text('Total Items:  '),
                              Text(listDetail['detail']['data']['goods_qrs'].length.toString(), style: TextStyle(color: API.colors['amarillo'], fontWeight: FontWeight.bold, fontSize: 18.0)),
                            ],
                          ),
                          
                          Row(
                            children: <Widget>[
                              Text('Scanned:  '),
                              Text(widget.delivered == 'Delivered' ? listDetail['detail']['data']['goods_qrs'].length.toString() :countProductChecked.toString(), style: TextStyle(color: API.colors['amarillo'], fontWeight: FontWeight.bold, fontSize: 18.0))
                            ],
                          )
                          
                        ],
                      ),
                    ),
                  
                    Expanded(
                      child: Center(
                        //este es el id del movimiento seleccionado
                        // child: Text("${widget.value}"),
                        child: ListView.builder(

                          itemCount: arreglo.length,
                          itemBuilder: (context, index) {
                            // aqui puedo declarar variables
                            // String serial = arreglo[index]['serial'] != null ? arreglo[index]['serial'] : 'No serial';
                            
                            return Padding(
                              padding: const EdgeInsets.only(top: 10.0, right: 15, left: 15),
                              child: ListTile(
                                title: Text(arreglo[index]['name'] + ' - ' + arreglo[index]['serialQR']),
                                trailing: widget.delivered == 'Delivered' ? null : Icon(
                                  arreglo[index]['check'] ? Icons.check_box : Icons.check_box_outline_blank,
                                  color:  arreglo[index]['check'] ? Colors.blue : null,
                                ),
                              ),
                            );

                          },
                        ),
                      ),
                      flex: 7,
                    ),
                  
                ],
              ),
            );
    }
  }
  
  Widget build(BuildContext context) {
    return Scaffold(
            appBar: AppBar(
               flexibleSpace: TopBar(hideBackButton: false),
            ),
            body: 
              _buildBody(),
              floatingActionButton: loading ? null : widget.delivered == 'Delivered' ? null  : AnimatedFloatingActionButton(
                fabButtons: <Widget>[
                  takeQr(), 
                  upload()
                ],
                colorStartAnimation: API.colors['amarillo'],
                colorEndAnimation: Colors.red,
                animatedIconData: AnimatedIcons.menu_close,
              ),
          );
  }

}
