import 'dart:convert';
import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'package:flutter/material.dart';

import 'createQr.dart';


void main() => runApp(GoodDetail());

class GoodDetail extends StatefulWidget {
  String goodId;
  String nameGood;
  String categoryGood;
  String subCategoryGood;

  GoodDetail({Key key, this.goodId, this.categoryGood, this.nameGood, this.subCategoryGood}) : super(key: key);

  @override
  _GoodDetailState createState() => _GoodDetailState();
}

class _GoodDetailState extends State<GoodDetail> {
  bool loading = true;
  var productsByGood;
  final price = TextEditingController();
 
  @override
  void initState() {
    super.initState();
    _getGoodDetail();
  }

  _getGoodDetail() {
    API.goodsById(widget.goodId).then((goods) async {
      var aux =  json.decode(goods.body);
      setState(() {
       productsByGood = aux['detail']['data'];
       loading = false; 
      });
     
    });

  }

  Widget _buildBody() {
    
    if(loading == true) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }else {
      if(productsByGood.length == 0) {
        return Column(
            children: <Widget>[
              Container(
            width: MediaQuery.of(context).size.width,
            height: 120.0,
            // color: Colors.greenAccent,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  width: 380.0,
                  height: 130.0,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      // begin: Alignment.topCenter,
                        colors: [Colors.black87, API.colors['amarillo']],
                      // stops: [0.0, 0.4]
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 1.0,
                        color: Colors.black12,
                        spreadRadius: 2.0
                      )
                    ]
                  ),

                  child: Container(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                                Text('Category: ' + widget.categoryGood,  style: TextStyle(fontSize: 16.0, color: Colors.white)),
                                Text('Subcategory: ' + widget.subCategoryGood,  style: TextStyle(fontSize: 16.0, color: Colors.white)),
                                Text('Name:  ' + widget.nameGood,  style: TextStyle(fontSize: 16.0, color: Colors.white))
                            ],
                          ),
                        ),

                          Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                          child:  Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              InkWell(
                                onTap: (){
                                  // _addQrModal();
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => CreateQr(goodId: widget.goodId,)));
                                },
                                child: Container(
                                  child: Icon(
                                    Icons.view_module,
                                    size: 32,
                                  ),
                                ),
                              ),
                              
                              Container(
                                alignment: Alignment.center,
                                child: Image.asset(
                                  'lib/src/assets/images/box_open.png',
                                  height: 70.0,
                                  ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
            ),
          ),

          Container(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('No data'),
              ),
            ),
          )
          ],
        );
      }else {
        return Container(
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 120.0,
                // color: Colors.greenAccent,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                      width: 380.0,
                      height: 130.0,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          // begin: Alignment.topCenter,
                            colors: [Colors.black87, API.colors['amarillo']],
                          // stops: [0.0, 0.4]
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 1.0,
                            color: Colors.black12,
                            spreadRadius: 2.0
                          )
                        ]
                      ),

                      child: Container(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                    Text('Category: ' + widget.categoryGood,  style: TextStyle(fontSize: 16.0, color: Colors.white)),
                                    Text('Subcategory: ' + widget.subCategoryGood,  style: TextStyle(fontSize: 16.0, color: Colors.white)),
                                    Text('Name:  ' + widget.nameGood,  style: TextStyle(fontSize: 16.0, color: Colors.white))
                                ],
                              ),
                            ),

                              Padding(
                              padding: const EdgeInsets.only(right: 15.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  InkWell(
                                    onTap: (){
                                      // _addQrModal();
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => CreateQr(goodId: widget.goodId,)));
                                    },
                                    child: Container(
                                      child: Icon(
                                        Icons.view_module,
                                        size: 32,
                                      ),
                                    ),
                                  ),
                                  
                                  Container(
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      'lib/src/assets/images/box_open.png',
                                      height: 70.0,
                                      ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                ),
              ),

              Expanded(
                flex: 5,
                child: Container(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0),
                  width: MediaQuery.of(context).size.width,
                  // color: Colors.blueAccent,
                  child: ListView.builder(
                    itemCount: productsByGood.length,
                    itemBuilder: (context, index) {
                        return(
                          Container(
                              // padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 6.0),
                                child: Text(productsByGood[index]['serialQR']),
                              ),
                            )
                        );
                    }
                  ),
                ),
              )
            ],
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
         flexibleSpace: TopBar(hideBackButton: true),
        ),
        body: 
        _buildBody(), 
      );
  }


}


