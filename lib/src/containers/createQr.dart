
import 'dart:convert';

import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'package:fino_inventory_mobile/src/containers/goodDetail.dart';
import 'package:fino_inventory_mobile/src/containers/movementsHistory.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:toast/toast.dart';


void main() => runApp(CreateQr());

class CreateQr extends StatefulWidget {
  String goodId;
  

  CreateQr({Key key, this.goodId}) : super(key: key);

  @override
  _CreateQrState createState() => _CreateQrState();
}

class _CreateQrState extends State<CreateQr> {
  bool loading = true;
  final _formKey = GlobalKey<FormState>();
  final formKey = new GlobalKey<FormState>();
  var goods = [];
  var goodValue;
  var goodInfo;
  var agencies = [];
  var agencieValue;
  var areas = [];
  var areaValue;
  var status = [{'name': 'New', 'value': 'N'}, {'name': 'Used', 'value': 'U'}, {'name':'Defective', 'value': 'D'}];
  var statusValue;
  var active = [{'name':'Yes' , 'value': true}, {'name': 'No', 'value': false}];
  var activeValue;
  final observation = TextEditingController();
  final serial = TextEditingController();
  final precio = TextEditingController();

  String areaTitle = 'Area';
  String goodTitle = 'Good';
  String agencyTitle = 'Agency';
  String activeTitle = 'Active';
  String statusTitle = 'Status';

  Color colorGood = API.colors['gray'], colorAgency = API.colors['gray'], colorArea = API.colors['gray'], colorPrice = API.colors['gray'];
 
  @override
  void initState() {
    super.initState();
    
    _getAgencies();
    if(widget.goodId != null) {
      _getGoodDetail();
    }else{
      _getGoods();
    }
    
  }

  _getGoodDetail() async {
    API.detailGood(widget.goodId).then((goods) async {
      dynamic aux =  json.decode(goods.body);
     
      setState(() {
       goodTitle = aux['detail']['data']['name'];
       goodValue = aux['detail']['data'];
       goodInfo = aux['detail']['data'];
       loading = false;
      });
    });

  }

  _getGoods() async {
    API.getGoodsList().then((response) {
      dynamic respuesta = json.decode(response.body);
      setState(() {
        goods = respuesta['detail']['data'];
        loading = false;
      });

    });
    
  }

  _getAgencies() {
    API.getAgencies().then((agency) async {
      var aux = json.decode(agency.body);
      setState(() {
        agencies = aux['detail']['data'];
        loading = false;
      });
    });    
  }

  _getAreas(value) {
    
    if(value['areas'].length == 0) {
      setState(() {
        areaTitle = 'No area';
        areas = [];
        // areaValue = null;
      });
    }else {
        areaTitle = 'Area';
        areas = value['areas'];
    }
    
  }

  _createQr() {
     setState(() {
      loading = true; 
    });
    

    if((goodValue != null) && (agencieValue != null) && (areaValue != null) && (precio.text != '') ) {
      var qr = [{
        'good': goodValue['id'],
        'agencyId': agencieValue['id'],
        'areaId': areaValue['id'],
        'isActive': activeValue != null ? activeValue['value'] : true,
        'status': statusValue != null ? statusValue['value'] : 'N',
        'serial': serial.text != '' ? serial.text : '',
        'observation': observation.text != '' ? observation.text : '',
        'price': double.parse(precio.text)
      }];
     
      API.createQr(qr).then((response) {
       
        if(response['detail']['success']) {
          setState(() {
            loading = false; 
          });
          Toast.show("Qr code created successfully", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
          Navigator.push(context, MaterialPageRoute(builder: (context) => GoodDetail(
            goodId: widget.goodId,
            nameGood: goodInfo['name'],
            categoryGood: goodInfo['category']['name'],
            subCategoryGood: goodInfo['subcategory']['name']
            )));
        }else{
          setState(() {
            loading = false; 
          });
          Toast.show("An error has occurred please try again", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);            
        }
      });
    }else {
      setState(() {
        if(goodValue == null) 
          colorGood = API.colors['redDanger'];
        
        if(agencieValue == null) 
          colorAgency = API.colors['redDanger'];
        
        if(areaValue == null) 
          colorArea = API.colors['redDanger'];
       
        if(precio.text == '') 
          colorPrice = API.colors['redDanger'];
      });

      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text("Fill in the required fields"),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  setState(() {
                   loading = false; 
                  });
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      );
    }


    
    
  }

  Widget _buildBody() {
    if(loading == true) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }else {
      return SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    children: <Widget>[
                      
                      //lista de goods
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: colorGood, width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: Text(goodTitle),
                              isExpanded: true,
                              items: goods.map<DropdownMenuItem>((value) {
                                return DropdownMenuItem(
                                  value: value,
                                  child: Text(value['name']),
                                );
                              }).toList(),
                              onChanged: (value) {
                                goodValue = value;
                                // goodTitle = value['good'];
                                setState(() {});
                              },
                              value: goodValue,
                            ),
                        ),
                      ),

                      //agencia
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: colorAgency, width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                            hint: Text(agencyTitle),
                            isExpanded: true,
                            items: agencies.map<DropdownMenuItem>((value) {
                              return DropdownMenuItem(
                                value: value,
                                child: Text(value['name']),
                              );
                            }).toList(),
                            onChanged: (value) {
                              _getAreas(value);
                              agencieValue = value;
                              agencyTitle = value['name'];
                              setState(() {});
                            },
                            value: agencieValue,
                          ),
                        ),
                      ),

                      //area
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: colorArea, width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                            hint: Text(areaTitle),
                            isExpanded: true,
                            items: areas.map<DropdownMenuItem>((value) {
                              return DropdownMenuItem(
                                value: value,
                                child: Text(value['name']),
                              );
                            }).toList(),
                            onChanged: (value) {
                              areaValue = value;
                              areaTitle = value['name'];
                              setState(() {});
                            },
                            value: areaValue,
                          ),
                        ),
                      ),

                      //status
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: API.colors['gray'], width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                            hint: Text(statusTitle),
                            isExpanded: true,
                            items: status.map<DropdownMenuItem>((value) {
                              return DropdownMenuItem(
                                value: value,
                                child: Text(value['name']),
                              );
                            }).toList(),
                            onChanged: (value) {
                              statusValue = value;
                              statusTitle = value['name'];
                              setState(() { });
                            },
                            value: statusValue,
                          ),
                        ),
                      ),

                      //active
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: API.colors['gray'], width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                            hint: Text(activeTitle),
                            isExpanded: true,
                            items: active.map<DropdownMenuItem>((value) {
                              return DropdownMenuItem(
                                value: value,
                                child: Text(value['name']),
                              );
                            }).toList(),
                            onChanged: (value) {
                              activeValue = value;
                              activeTitle = value['name'];
                              setState(() {});
                            },
                            value: activeValue,
                          ),
                        ),
                      ),

                      //serial
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(color: API.colors['gray'], width: 1.0,)
                            )
                          ),
                          child: TextFormField(
                            controller: serial,
                            decoration: InputDecoration(
                              hintText: 'Serial',
                            ),
                          ),
                        ),
                      ),

                      //precio
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(color: colorPrice, width: 1.0,)
                            )
                          ),
                          child: TextFormField(
                            controller: precio,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: 'Precio',
                            ),
                          ),
                        ),
                      ),

                      // observacion
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(color: API.colors['gray'], width: 1.0,)
                            )
                          ),
                          child:TextFormField(
                            controller: observation,
                            decoration: InputDecoration(
                              hintText: 'Observation',
                            ),
                          ),
                        ),
                      ),

                    
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          child: Text("Create qr"),
                          onPressed: () {
                            _createQr();
                          },
                        ),
                      ),

                    ],
                  ),
                ),
            ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
         flexibleSpace: TopBar(hideBackButton: false),
        ),
        body: 
        _buildBody(),
      );
  }

}


