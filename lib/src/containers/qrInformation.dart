
import 'dart:convert';

// import 'package:animated_floatactionbuttons/animated_floatactionbuttons.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
// import 'package:fino_inventory_mobile/src/containers/movementsHistory.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:toast/toast.dart';
import 'package:flutter/services.dart';


void main() => runApp(QrInformation());

class QrInformation extends StatefulWidget {
  @override
  _QrInformationState createState() => _QrInformationState();
}

class _QrInformationState extends State<QrInformation> {
  bool loading = false;
  bool isActive = false;
  bool custom = false;
  var name = 'No information';
  var price = 'No information';
  var information;
  var description;
  var url;
  var serialQr = 'No information';
 
  initState() {
    super.initState();
  }

  checkBarcode(barcode) {
    setState(() {
      loading = true;
    });
    
    
    var data = {
      'qr': barcode,
      'custom': custom
    };
    API.getUserInfo().then((user) async {
      var agencyId = json.decode(user.body);
      
      if(agencyId['detail']['success'] == 'False') {
        Toast.show("Session expired", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        final storage = new FlutterSecureStorage();
        await storage.deleteAll();
        Navigator.of(context).pushNamedAndRemoveUntil('Login', (Route<dynamic> route) => false);
      }else{
        API.checkQr(data).then((response) { 
          dynamic respuesta = json.decode(response.body);
          // print('\x1B[35m' + '=====================================' + '\x1B[0m');
          // print(respuesta);
          // print('\x1B[35m' + '=====================================' + '\x1B[0m');
          
          if(respuesta['detail']['data']['serialQR'] != null) {
            if(custom == true) {
              setState(() {
                isActive = respuesta['detail']['data']['isActive'] == true ? respuesta['detail']['data']['isActive'] : false;
                information = {'good': {'image': null}};
                name =  respuesta['detail']['data']['name'];
                description =  respuesta['detail']['data']['description'];
                serialQr = respuesta['detail']['data']['serialQR'];
                price = 'No information';
                loading = false;
              });
              print('aaaaaaaaaaaaaaaaaaaaa');
            }else {
              setState(() {
                isActive = respuesta['detail']['data']['isActive'] == true ? respuesta['detail']['data']['isActive'] : false;
                information = respuesta['detail']['data'];
                name =  respuesta['detail']['data']['good']['name'];
                price = respuesta['detail']['data']['price'].toString();
                url = respuesta['detail']['data']['good']['image'];
                serialQr = respuesta['detail']['data']['serialQR'];
                loading = false;
              });
            }
              

              // if(custom == true) {
              //   setState(() {
              //     name = respuesta['detail']['data']['name'];
              //     description =  respuesta['detail']['data']['description'];
              //   });
              // }
          }else {
            setState(() {
             loading = false; 
            });
            Toast.show("The scanned product was not found in the database", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          }

        });
      }
    });
}

  Future scan() async {
    try{
      // aqui entra cuando si scanea algo
      String barcode = await BarcodeScanner.scan();
      checkBarcode(barcode);
    } on PlatformException catch(e) {
      if(e.code == BarcodeScanner.CameraAccessDenied) {
        Toast.show("The user did not grant the camera permission", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        // El usuario no otorgó permiso a la cámara.
      }else {
        Toast.show("Unknow error: $e", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }
    } on FormatException {
      // aqui es cuando le doy al boton de atras sin scanear nada
    } catch(e) {
      Toast.show("Unknow error: $e", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }
  }

  Widget readQr() {
      return Container(
        child: FloatingActionButton(
          backgroundColor: API.colors['amarillo'],
          onPressed: (){
            scan();
          },
          heroTag: null,
          tooltip: 'Qr information',
          child: Icon(Icons.chrome_reader_mode),
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    
    
    return Scaffold(
        appBar: AppBar(
         flexibleSpace: TopBar(hideBackButton: true),
        ),
        body: 
          _buildBody(),
          floatingActionButton: loading ? null : FloatingActionButton(
            backgroundColor: API.colors['amarillo'],
            onPressed: (){
              scan();
            },
            child: Icon(Icons.center_focus_weak),
          ),
      );
  }


  Widget _buildBody() {
    if(loading == true) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }else {
      return Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment(0, 0),
                child: Text('QR information', style: TextStyle(fontSize: 24.0),),
              ),
              
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Is it a custom qr?'),
                  Switch(
                    value: custom,
                    onChanged: (value) {
                      setState(() {
                        custom = value;
                      });
                    },
                    activeTrackColor: API.colors['amarillo'], 
                    activeColor: API.colors['amarillo'],
                  ),
                ],
              ),
              
              Container(
                child: Text('Name: ' + name),
              ),
              Container(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text('SerialQR: ' + serialQr),
              ),
              custom ? 
              Container(
                padding: const EdgeInsets.only(top: 15.0),
                child:Text('Description: $description'),
              ) : Container(),

              !custom ? 
              Container(
                padding: const EdgeInsets.only(top: 15.0),
                child:Text('Active: $isActive'),
              ): Container(),

              !custom ? 
              Container(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text('Price: ' + price),
              ) : Container(),
              Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: information == null ? Container() : Container(
                  alignment: Alignment.center,
                  child: information['good']['image'] == null ? Icon(Icons.image, size: 50) : Image.network(information['good']['image']) 
                )
                
                // information != null ? Container(
                //   alignment: Alignment.center,
                //   child: information != null ? Image.network(information['good']['image']) : Icon(Icons.image, size: 50)
                // ) : null,
              )
            ],
          ),
        ),
      );
    }
  }
}

