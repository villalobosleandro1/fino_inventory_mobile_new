import 'dart:convert';

import 'package:animated_floatactionbuttons/animated_floatactionbuttons.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:fino_inventory_mobile/src/containers/createQr.dart';
import 'package:flutter/services.dart';
import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'package:fino_inventory_mobile/src/containers/dashboard.dart';
import 'package:fino_inventory_mobile/src/containers/registerMovements.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:toast/toast.dart';

import 'movementDetail.dart';

void main() {
  runApp(Center(child: MovementsHistory()));
}

class MovementsHistory extends StatefulWidget {
  String filterSelected;

  MovementsHistory({Key key, this.filterSelected}) : super(key: key);

  static get tag => null;
  _MovementHistoryState createState() => _MovementHistoryState();
}

class _MovementHistoryState extends State<MovementsHistory> {
  bool loading;
  int present = 0;
  int pendingMovements = 0;
  int totalItems = 0;
  int page = 1;
  String dropdownValue = 'All';

  var listMovements;
  var infoMovements;
  var agencyId;
  var filter = ['Sent', 'Received', 'Delivered'];

  @override
  void initState() {
    if(widget.filterSelected != null) {
      dropdownValue = widget.filterSelected;
    }

    super.initState();
    loading = true;
    _getHistory(widget.filterSelected);
  }

  _getHistory(value) {
   
    var data;
    API.getUserInfo().then((user) async {
      agencyId = json.decode(user.body);
      
      if(agencyId['detail']['success'] == 'False') {
        Toast.show("Session expired", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        final storage = new FlutterSecureStorage();
        await storage.deleteAll();
        // Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
        Navigator.of(context).pushNamedAndRemoveUntil('Login', (Route<dynamic> route) => false);
      }else {
        data = {
          'id': agencyId['detail']['data']['agency']['id'],
        };

        if(value != null) {
          data = {
            'id': agencyId['detail']['data']['agency']['id'],
            'filter': value
          };
        }

        infoMovements = {
          'type': 'list',
          'id': agencyId['detail']['data']['agency']['id'],
          'page': page
        };

        var info = {
            'id': agencyId['detail']['data']['agency']['id'],
            'filter': 'Sent'
        };

        API.getMovementsHistory(info).then((response) {
          var aux  = json.decode(response.body);
          pendingMovements = aux['count'] > 0 ? aux['count'] : 0;
        });
        
        API.getMovementsHistory(data).then((response) {
          listMovements  = json.decode(response.body);
          present = listMovements['results'].length;
          totalItems = listMovements['count'] > 0 ? listMovements['count'] : 0;
          setState(() {
            loading = false;
          });
          
        });

        
      }
      
    });
  }

  _loadMoreMovement() async {
    page++;
    infoMovements['page'] = page;

    API.getMovements(infoMovements).then((movements) {
      var aux = json.decode(movements.body);
      setState(() {
        listMovements['results'].addAll(aux['results']);
         present = listMovements['results'].length;
      });
    });
  }

  Widget noResults() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Container(
            alignment: Alignment.centerRight,
            child: DropdownButton<String>(
              value: dropdownValue,
              icon: Icon(Icons.filter_list),
              iconSize: 24,
              elevation: 16,
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue = newValue;
                });
                _getHistory(newValue);
              },
              items: <String>['Sent', 'Received', 'Delivered', 'All']
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                })
                .toList(),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
          child: Text('No movements were found to show', style: TextStyle(fontSize: 18.0 )),
        ),
        
        Center(
            child: Image.asset(
                API.image['noResults'],
                width: MediaQuery.of(context).size.width,
                height: 400,
                fit: BoxFit.contain,
            ),
        )
      ],
    );
  }

  _getIcon(item) {

    if(item['origin']['agency']['id'] == item['destination']['agency']['id']) {
      return Icons.compare_arrows;
    }else if(item['destination']['agency']['id'] == agencyId['detail']['data']['agency']['id']) {
      return Icons.call_received;
    }else {
      return Icons.call_made;
    }
  }

  _getColor(item) {

    if(item['origin']['agency']['id'] == item['destination']['agency']['id']) {
      return API.colors['amarillo'];
    }else if(item['origin']['agency']['id'] == agencyId['detail']['data']['agency']['id']) {
      return API.colors['greenSuccess'];
    }else {
      return API.colors['redDanger'];
    }
  }

  checkBarcode(barcode) {
    API.checkQr(barcode).then((response) { 
      dynamic respuesta = json.decode(response.body);
      
      if(respuesta['detail']['data']['serialQR'] != null) {
        bool isActive = respuesta['detail']['data']['isActive'] ? respuesta['detail']['data']['isActive'] : false;
        
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(respuesta['detail']['data']['good']['name']),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text(respuesta['detail']['data']['serialQR'])
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text('Active $isActive')
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Row(
                      children: <Widget>[
                        Text('Price: '),
                        Text(respuesta['detail']['data']['price'].toString())
                      ],
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: Container(
                      alignment: Alignment.center,
                      child: respuesta['detail']['data']['good']['image'] != null ? Image.network(respuesta['detail']['data']['good']['image']) : Icon(Icons.image, size: 50,)
                    ),
                  )
                ],
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: Text("Close"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );

      }else {
        Toast.show("The scanned product was not found in the database", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        Navigator.push(context, MaterialPageRoute(builder: (context) => MovementsHistory()));
      }

    });
}
  
  Future scan() async {
    try{
      // aqui entra cuando si scanea algo
      String barcode = await BarcodeScanner.scan();
      checkBarcode(barcode);
      // print(barcode);
    } on PlatformException catch(e) {
      if(e.code == BarcodeScanner.CameraAccessDenied) {
        Toast.show("The user did not grant the camera permission", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        // El usuario no otorgó permiso a la cámara.
      }else {
        Toast.show("Unknow error: $e", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }
    } on FormatException {
      // aqui es cuando le doy al boton de atras sin scanear nada
    } catch(e) {
      Toast.show("Unknow error: $e", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }
  }

  Widget createMovement() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: API.colors['amarillo'],
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterMovement()));
        },
        heroTag: null,
        tooltip: 'Create movements',
        child: Icon(Icons.add),
      ),
    );
}
  
  Widget readQr() {
      return Container(
        child: FloatingActionButton(
          backgroundColor: API.colors['amarillo'],
          onPressed: (){
            scan();
          },
          heroTag: null,
          tooltip: 'Good information',
          child: Icon(Icons.chrome_reader_mode),
        ),
      );
  }

  Widget createQr() {
      return Container(
        child: FloatingActionButton(
          backgroundColor: API.colors['amarillo'],
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => CreateQr()));
          },
          heroTag: null,
          tooltip: 'Create QR code',
          child: Icon(Icons.create_new_folder),
        ),
      );
  }
  
  Widget _buildBody() {
    if(loading) {
      return Container(
        color: Colors.white,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }else {
      if(totalItems == 0) {
        return noResults();
      }else {
        return Column(
          children: <Widget> [
            Padding(
              padding: const EdgeInsets.only(right: 10, left: 10),
              child: Row(
                // padd
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0, left: 8.0),
                      child: Row(
                        children: <Widget>[
                           Text('Pending movements: '),
                           Chip(
                              backgroundColor: API.colors['redDanger'],
                              padding: EdgeInsets.all(0),
                              label: Text('$pendingMovements'),
                           )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    // alignment: Alignment.centerRight,
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      icon: Icon(Icons.filter_list),
                      iconSize: 24,
                      elevation: 16,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                        });
                        _getHistory(newValue);
                      },
                      items: <String>['Sent', 'Received', 'Delivered', 'All']
                        .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        })
                        .toList(),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                child: ListView.builder(
                  itemCount: present,
                  itemBuilder: (context, index) {
                    return ( index == present-1 && present < totalItems) ?
                        Container(
                            child: FlatButton(
                                child: Text("Load More"),
                                onPressed: () {
                                  this._loadMoreMovement();
                                },
                            ),
                        ):

                        Container(
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => MovementDetail(
                                    value: listMovements['results'][index]['id'], 
                                    user: agencyId['detail']['data'],
                                    icono: _getIcon(listMovements['results'][index]),
                                    delivered: listMovements['results'][index]['status'],
                                  ),
                                ),
                              );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                              child: Container(
                                width: 380.0,
                                height: 130.0,

                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 1.0,
                                      color: Colors.black12,
                                      spreadRadius: 2.0
                                    )
                                  ]
                                ),

                                child: Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        // color: Colors.redAccent,
                                        // width: 290,
                                        // height: 130,
                                        child: Column(
                                          // crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Text('N. movement: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,)),
                                                    Text(listMovements['results'][index]['id_movement'].toString(),  style: TextStyle(fontSize: 16.0))
                                                  ],
                                                ),
                                              ],
                                            ),
                                            
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Text('Issuing user: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, )),
                                                    Text(listMovements['results'][index]['issuing_user']['first_name'], style:  TextStyle(fontSize: 16.0))
                                                  ],
                                                ),
                                              ],
                                            ),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Text('Receiving user: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, )),
                                                    Text(listMovements['results'][index]['receiving_user']['first_name'], style:  TextStyle(fontSize: 16.0))
                                                  ],
                                                ),
                                              ],
                                            ),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Text('Status:   ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, )),
                                                    Icon(listMovements['results'][index]['status'] == 'Delivered' ? Icons.done_all : Icons.done, 
                                                      color: listMovements['results'][index]['status'] == 'Delivered' ? Colors.blue : Colors.grey,
                                                    )
                                                  ],
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                        
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(right: 5.0),
                                        child: Container(
                                          // color: Colors.pinkAccent,
                                          // width: 70,
                                          // height: 130,
                                          alignment: Alignment.center,
                                          child: Icon(
                                            _getIcon(listMovements['results'][index]), 
                                            color: _getColor(listMovements['results'][index]),
                                            size: 50,
                                          ),
                                        ),
                                      )

                                      
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                  },
                ),
              ),
            )
          ]
        );
      }
      
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            flexibleSpace: TopBar(hideBackButton: false),
          ),
          body: _buildBody(),
          floatingActionButton: loading ? null : AnimatedFloatingActionButton(
                fabButtons: <Widget>[
                  createMovement(), 
                  // readQr(),
                  createQr()
                ],
                colorStartAnimation: API.colors['amarillo'],
                colorEndAnimation: Colors.red,
                animatedIconData: AnimatedIcons.menu_close,
              ),
        );
  }

}