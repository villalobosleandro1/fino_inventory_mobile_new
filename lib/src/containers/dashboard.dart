
import 'package:fino_inventory_mobile/src/containers/registerMovements.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:math';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'package:fino_inventory_mobile/src/containers/movementDetail.dart';
import 'package:toast/toast.dart';

void main() => runApp(Dashboard());

class Dashboard extends StatefulWidget {
  static get tag => null;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with SingleTickerProviderStateMixin {

  final storage = FlutterSecureStorage();
  var userInfo;
  var infoMovements;
  var infoAgencies;
  var subAreas = [];
  var agencies;
  var usersByAgency = [];
  bool loading;

  int page = 1;
  int present = 0;
  int totalItems = 0;

  dynamic listMovements;
  Animation<double> _angleAnimation;
  Animation<double> _scaleAnimation;
  AnimationController _controller;

  var originValue;
  var destinationValue;
  var destinationAreaValue;
  var receivingUserValue;

  String originTitle = 'Origin';
  String destinationTitle = 'Destination';
  String destinationAreaTitle = 'Destination Area';
  String receivingUserTitle = 'Choose a receiving user';


  initState() {
    super.initState();

    loading = true;
    _controller = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    _angleAnimation = Tween(begin: 0.0, end: 360.0).animate(_controller)
      ..addListener(() {
        setState(() {
        });
      });
    _scaleAnimation = Tween(begin: 1.0, end: 6.0).animate(_controller)
      ..addListener(() {
        setState(() {
        });
      });

    _angleAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (loading) {
          _controller.reverse();
        }
      } else if (status == AnimationStatus.dismissed) {
        if (loading) {
          _controller.forward();
        }
      }
    });

    _controller.forward();
    _getGoods();
    _getAgencies();
  }

  dispose() {
    _controller.dispose();
    super.dispose();
  }

  _getGoods() async {
    final storage = FlutterSecureStorage();
    // Map<String, String> allValues;

    // allValues = await storage.readAll();
    // var token = allValues['token'];


    API.getUserInfo().then((user) async {
      userInfo = json.decode(user.body);
      if(userInfo['detail']['success'] == 'False') {
        Toast.show("Session expired", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        
        await storage.deleteAll();
        // Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
        Navigator.of(context).pushNamedAndRemoveUntil('Login', (Route<dynamic> route) => false);
      }else {
        infoMovements = {
          'type': 'list',
          'id': userInfo['detail']['data']['agency']['id'],
          'page': page
        };
        API.getMovements(infoMovements).then((movements) {
          listMovements  = json.decode(movements.body);
          present = listMovements['results'].length;
          totalItems = listMovements['count'];
          loading = false;
        });
      }
     
    });
  }

  _getAgencies() {
    API.getAgencies().then((agency) {
      agencies = json.decode(agency.body);
      infoAgencies = agencies['detail']['data'];
    });
  }

  _loadMoreMovement() async {
    page++;
    infoMovements['page'] = page;

    API.getMovements(infoMovements).then((movements) {
      var aux = json.decode(movements.body);
      setState(() {
        listMovements['results'].addAll(aux['results']);
         present = listMovements['results'].length;
      });
    });
  }

  _getIcon(item) {
    // print(item);
    if(item['origin']['agency']['id'] == item['destination']['agency']['id']) {
      return Icons.compare_arrows;
    }else if(item['destination']['agency']['id'] == userInfo['detail']['data']['agency']['id']) {
      return Icons.call_received;
    }else {
      return Icons.call_made;
    }
  }

  Widget _buildBody() {
    if (loading) {
      return Center(
        child: _buildAnimation(),
      );
    } else {
      if(totalItems == 0) {
        return Column(
          // crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('No movements were found to show', style: TextStyle(fontSize: 18.0 )),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 12.0),
                child: Image.asset(
                    API.image['noResults'],
                    width: MediaQuery.of(context).size.width,
                    // height: MediaQuery.of(context).size.height,
                    fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        );
      }else {
        return Column (
          children: <Widget> [
            Expanded(
                child: ListView.builder(
                padding: const EdgeInsets.only(bottom: 10),
                itemCount: present,
                itemBuilder: (context, index) {
                  return ( index == present-1 && present < totalItems) ?

                    Container(
                        child: FlatButton(
                            child: Text("Load More"),
                            onPressed: () {
                              this._loadMoreMovement();
                            },
                        ),
                    )
                    :
                    InkWell(
                      splashColor: Colors.yellow,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MovementDetail(
                              value: listMovements['results'][index]['id'], 
                              user: userInfo['detail']['data'],
                              icono: _getIcon(listMovements['results'][index]),
                              delivered: listMovements['results'][index]['status']
                            ),
                          ),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10, right: 10, left: 10),
                        child: Container(
                          width: 380.0,
                          height: 130.0,

                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 1.0,
                                color: Colors.black12,
                                spreadRadius: 2.0
                              )
                            ]
                          ),

                          child: Container(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text('N. movement: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,)),
                                        Text(listMovements['results'][index]['id_movement'].toString(),  style: TextStyle(fontSize: 16.0))
                                      ],
                                    ),

                                    Icon(Icons.format_list_numbered_rtl, color: API.colors['amarillo']),
                                  ],
                                ),
                                
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text('Issuing user: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, )),
                                        Text(listMovements['results'][index]['issuing_user']['first_name'], style:  TextStyle(fontSize: 16.0))
                                      ],
                                    ),

                                    // SizedBox(width: 4.0),
                                    Icon(Icons.local_offer, color: API.colors['amarillo']),

                                  ],
                                ),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text('Receiving user: ', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, )),
                                        Text(listMovements['results'][index]['receiving_user']['first_name'], style:  TextStyle(fontSize: 16.0))
                                      ],
                                    ),
                                    // SizedBox(width: 4.0),
                                    Icon(Icons.supervised_user_circle, color: API.colors['amarillo']),

                                  ],
                                ),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Type movement', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                                    Icon(_getIcon(listMovements['results'][index]), color: API.colors['amarillo'])
                                  ],
                                ),

                                
                              ],
                            ),
                          ),
                        ),
                      ),
                    );

                },
              ),
            ),
            // Container(
            //   color: Colors.transparent,
            //   alignment: Alignment.centerRight,
            //   child: FloatingActionButton(
            //     onPressed: () {
            //       Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterMovement()));
            //     },
            //     child: Icon(Icons.add),
            //     backgroundColor: API.colors['amarillo']
            //   ),
            // )
          ]
        );
      }
    }
  }

  Widget _buildAnimation() {
    double circleWidth = 10.0 * _scaleAnimation.value;
    Widget circles = Container(
      width: circleWidth * 2.0,
      height: circleWidth * 2.0,
      child: Column(
        children: <Widget>[
          Row (
              children: <Widget>[
                _buildCircle(circleWidth, API.colors['amarillo']),
                _buildCircle(circleWidth, API.colors['verde']),
              ],
          ),
          Row (
            children: <Widget>[
              _buildCircle(circleWidth, API.colors['celeste']),
              _buildCircle(circleWidth, API.colors['rojo']),
            ],
          ),
        ],
      ),
    );

    double angleInDegrees = _angleAnimation.value;
    return Transform.rotate(
      angle: angleInDegrees / 360 * 2 * pi,
      child: Container(
        child: circles,
      ),
    );
  }

  Widget _buildCircle(double circleWidth, Color color) {
    return Container(
      width: circleWidth,
      height: circleWidth,
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              flexibleSpace: TopBar(hideBackButton: true),
            ),
            body:
              _buildBody(),
              // floatingActionButton: loading ? null : FloatingActionButton(
              //   onPressed: () {
              //     Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterMovement()));
              //   },
              //   tooltip: 'Create movement',
              //   child: Icon(Icons.add),
              //   backgroundColor: API.colors['amarillo']
              // )
          );
  }

  
}


