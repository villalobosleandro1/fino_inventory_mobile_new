import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:toast/toast.dart';

import 'package:fino_inventory_mobile/src/components/api.dart';
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'dashboard.dart';

void main() => runApp(RegisterMovement());

class RegisterMovement extends StatefulWidget {
  static get tag => null;

  @override
  _RegisterMovementState createState() => _RegisterMovementState();
}

class _RegisterMovementState extends State<RegisterMovement> {
  final _formKey = GlobalKey<FormState>();
  var originValue;
  var destinationValue;
  var destinationAreaValue;
  var receivingUserValue;
  var infoAgencies = [];
  var agencyOrigin = [];
  var agencies;
  var subAreas = [];
  var usersByAgency = [];
  var goods = [];
  var goodsIds = [];
  var userInfo;
  final observation = TextEditingController();

  var formatter = DateFormat('yyyy-MM-dd hh:mm');
  DateTime selectedDate = DateTime.now();
  bool loading;
  bool formIsValid;
  Color colorOrigin = API.colors['gray'], colorDestination = API.colors['gray'], colorArea = API.colors['gray'], colorReceiving = API.colors['gray'];
  String originTitle = 'Origin';
  String destinationTitle = 'Destination';
  String destinationAreaTitle = 'Destination Area';
  String receivingUserTitle = 'Choose a receiving user';

  List _myActivities;
  String _myActivitiesResult;
  final formKey = new GlobalKey<FormState>();

  initState() {
    loading = true;
    super.initState();
    _getAgencies();
    _myActivities = [];
    _myActivitiesResult = '';
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101)
    );
    if (picked != null && picked != selectedDate)
        setState(() {
          selectedDate = picked;
        });
  }

  _getAgencyWithUser() async {
    final storage = FlutterSecureStorage();
    Map<String, String> allValues;

    allValues = await storage.readAll();
    var agencyID = allValues['agencyID'];
    
    API.getInfoAgency(agencyID).then((agency){
      var aux = json.decode(agency.body);

      agencyOrigin.add(aux['detail']['data']);
      originTitle = aux['detail']['data']['name'];
      originValue = aux['detail']['data'];
      setState(() {
        loading = false;
      });
    });

    _getGoodsForAgency({'id': agencyID});
  }
  
  _getAgencies() {

    API.getUserInfo().then((user) async {
      var userInfo = json.decode(user.body);
     
      if(userInfo['detail']['success'] == 'False') {
        Toast.show("Session expired", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        final storage = new FlutterSecureStorage();
        await storage.deleteAll();
        Navigator.of(context).pushNamedAndRemoveUntil('Login', (Route<dynamic> route) => false);
      }else {
        if(userInfo['detail']['data']['role'] == 'User') {
              _getAgencyWithUser();
        }
        API.getAgencies().then((agency) async {
            agencies = json.decode(agency.body);
            infoAgencies = agencies['detail']['data'];
            if(userInfo['detail']['data']['role'] != 'User') {
              agencyOrigin = agencies['detail']['data'];
            }
            
            setState(() {
              loading = false;
            });
        });
       
        
      }
    });

    
  }

  _getUserByAgency() {
    
    API.getUserByAgency(destinationValue['id']).then((response) async {
      var respuesta =  await json.decode(response.body);
      setState(() {
        usersByAgency = respuesta['detail']['data'];
      });
    });

  }

  _getAreas(value) {

    destinationAreaTitle = 'Destination Area';
    receivingUserTitle = 'Choose a receiving user';
    setState(() {
      subAreas =[];
      usersByAgency = [];
      destinationValue = value;
      destinationTitle = value['name'];
    });
    
    // destinationAreaValue = [];
    // receivingUserValue = [];

    subAreas = value['areas'];
    if(value['areas'].length == 0) {
      destinationAreaTitle = 'Destination Area';
    }

    //esta parte se hizo porque en el backend hay una validacion de que solo se pueden
    //crear movimientos a las subareas que esten definidas como "isWarehouse"
    //cuando se modifique en el backend se debe quitar esta validacion para que salgan todas
    //las subareas de cada agencia
    
    if(originValue['id'] == value['id']) {
      subAreas = value['areas'];
    }else {
      var aux = [];
      for(var i = 0; i < value['areas'].length; i++) {
        if(value['areas'][i]['isWarehouse'] == true) {
          aux.add(value['areas'][i]);
        }
      }

      setState(() {
        subAreas = aux;
      });

    }
       
    //hasta aqui

    
  }

  _getGoodsForAgency(value) {
    API.getQrForAgencyId(value['id']).then((response) {
      dynamic respuesta = json.decode(response.body);

      setState(() {
        goods = respuesta['detail']['data'];
      });

    });
  }

  _createMovement() {

    setState(() {
      loading = true; 
    });
    String formatted = formatter.format(selectedDate);
    
    if((originValue != null) && (destinationValue != null) && (destinationAreaValue != null) && (receivingUserValue != null) && (_myActivities.length > 0)) {
      bool isIntern = originValue['id'] == destinationValue['id'] ? true : false;
      var informacion = {
        'type': 'POST',
        'data': {
          'origin': {
              'agency':{
                  'id': originValue['id']
              }
          },
          'destination': {
              'agency':{
                  'id': destinationValue['id'],
                  'area': {
                      'id': destinationAreaValue['id']
                  }
              }
        },
        'is_intern': isIntern,
        'receiving_user':{
              'id': receivingUserValue['id']
        },
        'shipping_date': formatted,
        'goods_qr': _myActivities,
        'observation': observation.text
        }
      };

      // print('information $informacion');

      API.createMovements(informacion).then((movements) {
        if(movements['detail']['success']) {
          setState(() {
            loading = false; 
          });
          Toast.show("Movement created successfully", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
          Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
        }else{
          setState(() {
            loading = false; 
          });

          Toast.show("An error has occurred please try again", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);            
        }
      });
      
    }else {
      setState(() {
        if(originValue == null) 
          colorOrigin = API.colors['redDanger'];
        
        if(destinationValue == null) 
          colorDestination = API.colors['redDanger'];
        
        if(destinationAreaValue == null) 
          colorArea = API.colors['redDanger'];
       
        if(receivingUserValue == null) 
          colorReceiving = API.colors['redDanger'];
       
      });

      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text("Fill in the required fields"),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  setState(() {
                   loading = false; 
                  });
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      );
      
    }

  }

  _buildBody() {
    if(loading) {
       return Container(
        color: Colors.white,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }else {
      return SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    children: <Widget>[
                      
                      //agencia de donde sale el movimiento
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: colorOrigin, width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: Text(originTitle),
                              isExpanded: true,
                              items: agencyOrigin.map<DropdownMenuItem>((value) {
                                return DropdownMenuItem(
                                  value: value,
                                  child: Text(value['name']),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                _getGoodsForAgency(newValue);
                                originValue = newValue;
                                originTitle = newValue['name'];
                                setState(() {});
                              },
                              value: originValue,
                            ),
                        ),
                      ),

                      //agencia que recibe el movimiento
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: colorDestination, width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                            hint: Text(destinationTitle),
                            isExpanded: true,
                            items: infoAgencies.map<DropdownMenuItem>((value2) {
                              return DropdownMenuItem(
                                value: value2,
                                child: Text(value2['name']),
                              );
                            }).toList(),
                            onChanged: (newValue2) {
                              _getAreas(newValue2);
                            },
                            value: destinationValue,
                          ),
                        ),
                      ),

                      //subareas
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: colorArea, width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                            value: null,
                            hint: Text(destinationAreaTitle),
                            isExpanded: true,
                            items: subAreas.map<DropdownMenuItem>((value3) {
                              return DropdownMenuItem(
                                value: value3,
                                child: Text(value3['name']),
                              );
                            }).toList(),
                            onChanged: (newValue3) {
                              destinationAreaValue = newValue3;
                              destinationAreaTitle = newValue3['name'];
                               _getUserByAgency();
                            },
                            // value: destinationAreaValue,
                          ),
                        ),
                      ),

                      //usuario que recibe
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: colorReceiving, width: 1.0,)
                          )
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                            value: null,
                            hint: Text(receivingUserTitle),
                            isExpanded: true,
                            items: usersByAgency.map<DropdownMenuItem>((value4) {
                              return DropdownMenuItem(
                                value: value4,
                                child: Text(value4['first_name'] + ' ' + value4['last_name']),
                              );
                            }).toList(),
                            onChanged: (newValue4) {
                              receivingUserValue = newValue4;
                              receivingUserTitle = newValue4['first_name'] + ' ' + newValue4['last_name'];
                              setState(() {});
                            },
                          ),
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: ButtonTheme(
                          buttonColor: Colors.white,
                          minWidth: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            onPressed: () => _selectDate(context),
                            child: Text("Select date", ),
                          ),
                        ),
                      ),
                      
                      Text("${selectedDate.toLocal()}"),

                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: observation,
                          decoration: InputDecoration(
                            hintText: 'Observation',
                          ),
                        ),
                      ),

                      Center(
                        child: Form(
                          key: formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top: 8),
                                child: MultiSelectFormField(
                                  autovalidate: false,
                                  titleText: 'Serials QR',
                                  validator: (value) {
                                    if (value == null || value.length == 0) {
                                      return 'Please select one or more goods';
                                    }
                                  },
                                  dataSource: goods,
                                  textField: 'serialQR',
                                  valueField: 'serialQR',
                                  okButtonLabel: 'OK',
                                  cancelButtonLabel: 'CANCEL',
                                  required: true,
                                  hintText: 'Please choose one or more',
                                  value: _myActivities,
                                  onSaved: (value) {
                                    setState(() {
                                      _myActivities = value;
                                    });
                                  },
                                ),
                              ),

                              Container(
                                padding: EdgeInsets.all(16),
                                child: Text(_myActivitiesResult),
                              )
                            ],
                          ),
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          child: Text("Submit"),
                          onPressed: () {
                            _createMovement();
                          },
                        ),
                      ),

                    ],
                  ),
                ),
            ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
         flexibleSpace: TopBar(hideBackButton: false),
        ),
        body: _buildBody(),
      );
  }
}
