
import 'package:fino_inventory_mobile/src/components/topBar.dart';
import 'package:flutter/material.dart';


void main() => runApp(Menu());

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  bool loading;
 
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    
    return Scaffold(
        appBar: AppBar(
         flexibleSpace: TopBar(hideBackButton: true),
        ),
        body: 
        _buildBody(), 
      );
  }


  Widget _buildBody() {
    if(loading == true) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }else {
      double height = (MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top - kToolbarHeight) / 4;
      
      return SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              height: height,
                child: InkWell(
                  
                  onTap: (){
                    Navigator.of(context).pushNamedAndRemoveUntil('RegisterMovement', (Route<dynamic> route) => false);
                  },
                  child: Container(
                  alignment: AlignmentDirectional(0, 0),
                  margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors. black54,
                        offset: Offset(0.0, 10.0),
                        blurRadius: 10.0
                      )
                    ]
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.add,
                        size: 50,
                      ),
                      Text('Create Movement')
                    ],
                  ),
             
                ),
              ),
              
            ),

            Container(
              // color: Colors.greenAccent,
              height: height,
                child: InkWell(
                  onTap: (){
                    Navigator.of(context).pushNamedAndRemoveUntil('MovementsHistory', (Route<dynamic> route) => false);
                  },
                  child: Container(
                  alignment: AlignmentDirectional(0, 0),
                  margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors. black54,
                        offset: Offset(0.0, 10.0),
                        blurRadius: 10.0
                      )
                    ]
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.history,
                        size: 50,
                      ),
                      Text('List Movements')
                    ],
                  ),
             
                ),
              ),
            ),

            Container(
              // color: Colors.greenAccent,
              height: height,
                child: InkWell(
                  onTap: (){
                    Navigator.of(context).pushNamedAndRemoveUntil('ListGoods', (Route<dynamic> route) => false);
                  },
                  child: Container(
                    // color: Colors.white10,
                  alignment: AlignmentDirectional(0, 0),
                  margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors. black54,
                        offset: Offset(0.0, 10.0),
                        blurRadius: 10.0
                      )
                    ]
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.list,
                        size: 50,
                      ),
                      Text('List Goods')
                    ],
                  ),
             
                ),
              ),
              
            ),

            Container(
              // color: Colors.greenAccent,
              height: height,
                child: InkWell(
                  onTap: (){
                    Navigator.of(context).pushNamedAndRemoveUntil('QrInformation', (Route<dynamic> route) => false);
                  },
                  child: Container(
                    // color: Colors.white10,
                  alignment: AlignmentDirectional(0, 0),
                  margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors. black54,
                        offset: Offset(0.0, 10.0),
                        blurRadius: 10.0
                      )
                    ]
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.assignment,
                        size: 50,
                      ),
                      Text('Qrs Information')
                    ],
                  ),
             
                ),
              ),
              
            ),
          ],
        ),
      );
    }
  }
}


