
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';

import 'package:fino_inventory_mobile/src/containers/dashboard.dart';
import 'package:fino_inventory_mobile/src/components/logo.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fino_inventory_mobile/src/components/api.dart';


class Login extends StatefulWidget {
  static String tag = 'login-page';
  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<Login> with TickerProviderStateMixin {

  RegExp emailRegExp = RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  GlobalKey<FormState> _key = GlobalKey();
  bool loading;
  bool animationAux = true;
  final email = TextEditingController();
  final password = TextEditingController();
  final logo = Logo(name: API.image['logo']);
  var animationStatus = 0;

  
  @override
  void initState() {
    loading = false;
    super.initState();
  }

  @override
  void dispose(){
    email.dispose();
    password.dispose();
    super.dispose();
  }

  timeToAlert() {
    setState(() {
      loading = false; 
    });

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text("The consultation has taken a long time please try later"),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }
  
  void conectar() async { 
      print('entro $loading');
      var bytes = utf8.encode(password.text); 
      var digest = sha512.convert(bytes).toString();
      
      API.login(email.text, digest).then((response) async {
        var respuesta = json.decode(response.body);
        // print('\x1B[35m' + '=====================================' + '\x1B[0m');
        // print(respuesta);
        // print('\x1B[35m' + '=====================================' + '\x1B[0m');
        
        if(respuesta['detail']['success']) {
          final storage = FlutterSecureStorage();
          await storage.write(key: 'token', value: respuesta['detail']['data']['token']);
          await storage.write(key: 'agencyID', value: respuesta['detail']['data']['agency']['id']);
          // Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
          // Navigator.of(context).pushNamed('');
          Navigator.of(context).pushNamedAndRemoveUntil('Menu', (Route<dynamic> route) => false);
        }else{
          setState(() {
            loading = false; 
          });
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Error Username/email or password incorrect"),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            }
          );
        }
      });
  }

  // void validateEmail() {
  //   setState(() {
  //     animationAux = true; 
  //   });
  // }

  Future<bool> _exitApp() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit this application?'),
        content: Text('We hate to see you leave...'),
        actions: <Widget>[
          FlatButton(
            child: Text('No'),
            onPressed: () => Navigator.of(context).pop(false),
          ),
          FlatButton(
            child: Text('Yes'),
            onPressed: () => Navigator.of(context).pop(true),
            // onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil('/screen4', (Route<dynamic> route) => false)
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // final _formKey = GlobalKey<FormState>();
    return WillPopScope(
      // key: _formKey,
      onWillPop:  _exitApp,
      child: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (loading) {
      return Container(
        color: Colors.white,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }else {
      return Scaffold(
              body: Column(
                  children: <Widget>[
                    logo,
                    
                    Form(
                      key: _key,
                      child: Column(
                        children: <Widget>[
                          
                        Padding(
                          padding: const EdgeInsets.only(left: 40.0, right: 40.0, top: 20),
                          child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    width: 1.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              child: TextFormField(
                                textCapitalization: TextCapitalization.none,
                                validator: (text) {
                                  if (text.length == 0) {
                                    return "Email field is required";
                                  } else if (!emailRegExp.hasMatch(text)) {
                                    return "The format of the email is not valid";
                                  }
                                  return null;
                                },
                                //  onChanged: (text) {
                                //   print("First text field: $text");
                                //   setState(() {
                                //    animationAux = false; 
                                //   });
                                //   var timer = Timer(Duration(milliseconds: 1500), this.validateEmail);


                                  

                                  
                                  
                                // },
                                keyboardType: TextInputType.emailAddress,
                                controller: email,
                                textAlign: TextAlign.center, 
                                style: const TextStyle(
                                  color: Colors.black,
                                ),
                                
                                decoration: InputDecoration(
                                  
                                  icon: Icon(Icons.email, size: 32.0, color: Color.fromRGBO(252, 199, 19, 1.0)),
                                  border: InputBorder.none,
                                  hintText: 'Email',
                                  hintStyle: const TextStyle(color: Colors.black, fontSize: 15.0),
                                  contentPadding: const EdgeInsets.only( top: 20.0, right: 30.0, bottom: 20.0, left: 5.0),
                                ),
                              ),
                            ),
                        ),
          
                      
                          Padding(
                            padding: const EdgeInsets.only(left: 40.0, right: 40.0, top: 10),
                            
                            child: Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                      width: 1.0,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),

                              child: TextFormField(
                                
                                  validator: (text) {
                                    if (text.length == 0) {
                                      return "Campo password es requerido";
                                    }else if (text.length < 5) {
                                      return "Su contraseña debe ser al menos de 5 caracteres";
                                    } 
                                    return null;
                                  },
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.none,
                                  controller: password,
                                  obscureText: true,
                                  textAlign: TextAlign.center,

                                  style: const TextStyle(
                                    color: Colors.black,
                                  ),

                                  decoration: InputDecoration(
                                    icon: Icon(Icons.lock, size: 32.0, color: Color.fromRGBO(252, 199, 19, 1.0)),
                                    border: InputBorder.none,
                                    hintText: 'Password',
                                    // labelText: 'Contraseña',
                                    hintStyle: const TextStyle(color: Colors.black, fontSize: 15.0),
                                    contentPadding: const EdgeInsets.only(top: 20.0, right: 30.0, bottom: 20.0, left: 5.0),
                                  ),
                                  
                                ),
                            ),
                          ),

                        

                          
                            Padding(
                              padding: const EdgeInsets.only(left: 40.0, right: 40.0, top: 30),
                              child: InkWell(
                                onTap: () async {
                                  if (_key.currentState.validate()) {
                                    setState(() {
                                     loading = true; 
                                    });
                                    _key.currentState.save();
                                    this.conectar();
                                  }
                                },
                                child: Container(
                                width: 320.0,
                                height: 60.0,
                                alignment: FractionalOffset.center,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: <Color>[const Color.fromRGBO(229, 162, 164, 1.0), const Color.fromRGBO(252, 199, 19, 1.0)],
                                  ),
                                  // color: const Color.fromRGBO(252, 199, 19, 1.0),
                                  borderRadius: BorderRadius.all(const Radius.circular(30.0)),
                                ),
                                child: Text(
                                  "Sign In",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w300,
                                    letterSpacing: 0.3,
                                  ),
                                  
                                ),
                              ),
                              ),
                            )
                            
                        
                          ],
                        ),
                    )
                  ],
                ),
      );
    }
    

  }
}